package pkg003_.enter_two_numbers;

import java.util.Scanner;

public class Enter_Two_Numbers {

    public static void main(String[] args) throws InterruptedException {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the first number");
        int a = sc.nextInt();
        System.out.println("Enter the second number");
        int b = sc.nextInt();
        System.out.println("System is calculating....");
        Thread.sleep(1500);
        System.out.println("Result of adding is: " +(a+b));
        
    }

}