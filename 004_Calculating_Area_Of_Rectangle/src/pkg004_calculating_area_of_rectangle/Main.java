package pkg004_calculating_area_of_rectangle;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert height");
        int height = sc.nextInt();
        System.out.println("Insert width");
        int width = sc.nextInt();
        System.out.println("System is calculating.....");
        Thread.sleep(1500);
        System.out.println("Area of rectangle is: " + (height*width));
        System.out.println("good bye");
    }

}
