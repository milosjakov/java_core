package pkg005_simple_string_application;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Insert your first name");
        String first_name = sc.nextLine();
        System.out.println("Insert your last name");
        String last_name = sc.nextLine();
        System.out.println("Checking.... \nPlease Wait a moment");
        Thread.sleep(2000);
        System.out.println(first_name +" "+last_name +" "+"welcome to our System.");
        
        
    }

}
